# set vars


Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools

Import-Module ADDSDeployment

Install-ADDSForest `
	-DomainName "jdogg.local" `
	-CreateDnsDelegation:$false `
	-DatabasePath "C:\Windows\NTDS" `
	-DomainMode "7" `
	-DomainNetbiosName "jdogg" `
	-ForestMode "7" `
	-InstallDns:$True `
	-LogPath "C:\Windows\NTDS" `
	-NoRebootOnCompletion:$True `
	-SysvolPath "C:\Windows\SYSVOL"

#notes
# DomainMode / ForestMode = 7: Windows2016Forest
# 
