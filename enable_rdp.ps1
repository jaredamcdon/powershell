# registry edit  @ fDenyTSConnections -> 0 = enabled RDP
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -Name "fDenyTSConnections" -Value 0

# enable rdp through firewall
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"
