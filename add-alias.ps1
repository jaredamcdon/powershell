$aliasName = Read-Host "New Alias name"
$aliasPath = Read-Host "New Alias path"
$aliasString = "New-Alias $aliasName $aliasPath"

Add-Content -path C:\Windows\System32\WindowsPowerShell\v1.0\profile.ps1 -Value "$aliasString"
