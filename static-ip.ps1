$ip = "10.0.0.55"
$prefix = "16"
$GW = "10.0.0.1"
$dnsList = @("10.0.0.1", "1.1.1.1", "1.0.0.1", "127.0.0.1")
$adapter = (Get-NetAdapter).ifIndex
try {
	Write-Host $ErrorActionPreference
	New-NetIPAddress -IPAddress $ip -PrefixLength $prefix -InterfaceIndex $adapter -DefaultGateway $GW -ErrorAction Stop
}
catch {
	Remove-NetIPAddress -InterfaceIndex $adapter
	Remove-NetRoute -InterfaceIndex $adapter
	New-NetIPAddress -IPAddress $ip -PrefixLength $prefix -InterfaceIndex $adapter -DefaultGateway $GW 
}

Set-DnsClientServerAddress -InterfaceIndex $adapter -ServerAddresses $dnsList
