# Windows Powershell
> set of Windows Powershell Scripts for Windows Server

## Notes
- Many scripts do not work
  - Could require to be run manually
    - Has to do with what "user" is running the commands
    - Powershell scripts not always run as the user running the script
  - Could require direct/RDP access
    - Programs utilizing GUI prompts (i.e. msiexec) require local access
      - Cannot be ran using Powershell over SSH
