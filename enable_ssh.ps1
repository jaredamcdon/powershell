$ssh_ver=(Get-WindowsCapability -Online -Name OpenSSH.Server*)
$ssh_client=(Get-WindowsCapability -Online -Name OpenSSH.Client*)

Add-WindowsCapability -Online -Name $ssh_ver
Add-WindowsCapability -Online -Name $ssh_client

Set-Service -Name sshd -StartupType Automatic
Set-Service -Name ssh-agent -StartupType Automatic

$sshd_status=(Get-Service sshd).Status
$ssh_agent=(Get-Service ssh-agent).Status

Write-Host "sshd:`t`t$sshd_status"
Write-Host "ssh-agent:`t$ssh_agent_status"